function matchesperyear(matches){
    let result = {};
    for (let i=0;i<matches.length;i++){
        if (matches[i].season in result){
            result[matches[i].season] +=1
        }else{
            result[matches[i].season] =1
        }
    }
    return result;
}


function matchesownperyear(matches){
    let result ={};
    for (let i=0;i<matches.length;i++){
        if (matches[i].season in result){
            if (matches[i].winner in result[matches[i].season]){
                result[matches[i].season][matches[i].winner] +=1
            }else{
                result[matches[i].season][matches[i].winner] =1
            }
        }else{
            result[matches[i].season] ={}
            result[matches[i].season][matches[i].winner] =1
        }
    }
    return result;
}


function extraruns2016(x,y){
    let result = {};
    let arr = [];
    for (let i=0;i<x.length;i++){
        if (x[i].season === 2016){
            arr.push(x[i].id)
        }
    }
    let startmatch = Math.min(...arr)
    let endmatch = Math.max(...arr)
    for (let i =0;i<y.length;i++){
        if (y[i].match_id >=startmatch && y[i].match_id <= endmatch){
            if (y[i].bowling_team in result){
                result[y[i].bowling_team] += y[i].extra_runs
            }else{
                result[y[i].bowling_team] = y[i].extra_runs
            }
        }
    }
    return result;
}


function topTenEconomy(x,y){
    let arr = [];
    for (let i=0;i<x.length;i++){
        if (x[i].season === 2015){
            arr.push(x[i].id)
        }
    }
    let totalBalls = {};
    let totalRuns = {};
    for (let i=0;i<y.length;i++){
        if (arr.includes(y[i].match_id) ){
            if (y[i].bowler in totalBalls && y[i].bowler in totalRuns){
                totalBalls[y[i].bowler] +=1
                totalRuns[y[i].bowler] +=parseInt(y[i].total_runs)
            }else{
                totalBalls[y[i].bowler] = 1
                totalRuns[y[i].bowler] =parseInt(y[i].total_runs)
            }
        }
    }
    let totalOvers = {};
    for (let player in totalBalls){
        if (totalBalls[player] % 6 === 0){
            totalOvers[player] = totalBalls[player] /6;
        }else{
            let extraBall = totalBalls[player] % 6;
            totalOvers[player] = ((totalBalls[player]) / 6 + extraBall/6).toFixed(2);
        }
    }
    let bowlerArray = [];
    for (let value in totalOvers, totalRuns) {
        bowlerArray.push([value, (totalRuns[value] / totalOvers[value]).toFixed(2)]);
    }
    bowlerArray.sort(function (bowler1,bowler2) {
        return bowler1[1]-bowler2[1]
    });
    let economicalObj = bowlerArray.slice(0, 10);
    let economybowlers = {};
    for(let i=0;i<economicalObj.length;i++){
        if (economicalObj[i] in economybowlers){
            economybowlers[economicalObj[i][0]] +=  economicalObj[i][1]
        }else{
            economybowlers[economicalObj[i][0]] = economicalObj[i][1]
        }
    }
    return economybowlers;
}

//Extra problems

function wonthetossandmatch(x){
    let times = {}
    for (let i=0;i<x.length;i++){
        if (x[i].toss_winner === x[i].winner){
            if (x[i].toss_winner in times){
                times[x[i].toss_winner] +=1
            }else{
                times[x[i].toss_winner]  =1
            }
        }
    }
    return times;
}

function highestplayerofmatch(x){
    let awards = {}
    for(let i=0;i<x.length;i++){
        if (x[i].season in awards){
            if (x[i].player_of_match in awards[x[i].season]){
                awards[x[i].season][x[i].player_of_match] +=1
            }else{
                awards[x[i].season][x[i].player_of_match] =1
            }
        }else{
            awards[x[i].season] ={}
            awards[x[i].season][x[i].player_of_match] =1
        }
    }
    let obj={};
    for (let i in awards){
        let key = Math.max(...Object.values(awards[i]))
        obj[i] ={};
        for(let j in awards[i]){
            if (awards[i][j] === key){
                obj[i][j]=key
            }
        }
    }
    return obj;
}





function strikerate(x,y){
    let obj = {};
        for (let i of y) {
            let wide = parseInt(i.wide_runs);
            let noball = parseInt(i.noball_runs);
            for (let j of x) {
                if (i.match_id == j.id) {
                    if (!(obj.hasOwnProperty(i.batsman))) {
                        obj[i.batsman] = {}
                        obj[i.batsman][j.season] = {runs: 0, balls: 0 };
                        obj[i.batsman][j.season]["runs"] += i.batsman_runs;
                        if (wide === 0 && noball === 0) {
                            obj[i.batsman][j.season]["balls"] += 1;
                        }
                    }
                    else if (obj.hasOwnProperty(i.batsman)) {
                        if (obj[i.batsman].hasOwnProperty(j.season)) {
                            obj[i.batsman][j.season]["runs"] += i.batsman_runs;
                            if (wide == 0 && noball == 0) {
                                obj[i.batsman][j.season]["balls"] += 1;
                            }
                        }
                        else if (!(obj[i.batsman].hasOwnProperty(j.season))) {
                            obj[i.batsman][j.season] = { runs: 0, balls: 0 };
                            obj[i.batsman][j.season]["runs"] += i.batsman_runs;
                            if (wide == 0 && noball == 0) {
                                obj[i.batsman][j.season]["balls"] += 1;
                            }
                        }
                    }
                }
            }
        }
        let eachPlayerStrikeRate={};
        for(let eachPlayer in obj){
            let eachPlayerYear={};
            for(let eachYear in obj[eachPlayer]){
                eachPlayerYear[eachYear]=parseFloat((((obj[eachPlayer][eachYear]["runs"])/(obj[eachPlayer][eachYear]["balls"]))*100).toFixed(3));
            }
            eachPlayerStrikeRate[eachPlayer]=eachPlayerYear;
        }
        return eachPlayerStrikeRate;
}
    
function superovereco(y){
    let bowlers = {}
    for (let i of y){
        if (i.is_super_over){
            if(!(i.bowler in bowlers)){
                bowlers[i.bowler] = {runs:0,balls:0};
                bowlers[i.bowler]["runs"] += i.total_runs - i.bye_runs-i.legbye_runs-i.penalty_runs
                if (i.wide_runs === 0 && i.noball_runs === 0){
                    bowlers[i.bowler]["balls"] +=1
                }
            }else if (i.bowler in bowlers){
                bowlers[i.bowler]["runs"] += i.total_runs - i.bye_runs-i.legbye_runs-i.penalty_runs
                if (i.wide_runs === 0 && i.noball_runs === 0){
                    bowlers[i.bowler]["balls"] +=1
                }
            }
        }
    }
    let bowlereco = {};
    for (let i in bowlers){
        bowlereco[i] = parseFloat(((bowlers[i]["runs"]/bowlers[i]["balls"])*6).toFixed(3))
    }
    const  bestBowlereco = Object.entries(bowlereco).sort(function (a,b){
        return a[1]-b[1]
    })
    return bestBowlereco[0];
}


function highestDismissal(y){
    let highest = {};
    for (let i of y){
        let bowler = i.bowler;
        let playerdismissed = i.player_dismissed;
        if (playerdismissed !== "" && playerdismissed !== 'run out' && 
        playerdismissed !== 'retired hurt' && playerdismissed !== 'obstructing the field' && 
        playerdismissed !== 'hit wicket'){
            if (!(highest[playerdismissed])){
                highest[playerdismissed] = {};
                highest[playerdismissed][bowler] =1;
            }else if (highest[i.player_dismissed]){
                if (highest[playerdismissed][bowler]){
                    highest[playerdismissed][bowler] +=1
                }else{
                    highest[playerdismissed][bowler] =1
                }
            }
        }
    }
    let arr=[];
    for (let playerdismissed in highest){
        for (let bowler in highest[playerdismissed]){
            let arr1=[];
            arr1.push(playerdismissed)
            arr1.push(bowler)
            arr1.push(highest[playerdismissed][bowler])
            arr.push(arr1);
        }
    }
    let arr3 =arr.sort(function (a,b){
        return b[2]-a[2];

    });
    return arr3[0];
}







module.exports.matchesperyear=matchesperyear;
module.exports.matchesownperyear=matchesownperyear;
module.exports.extraruns2016=extraruns2016;
module.exports.topTenEconomy=topTenEconomy;

//Extra problems
module.exports.wonthetossandmatch=wonthetossandmatch;
module.exports.highestplayerofmatch=highestplayerofmatch;
module.exports.strikerate=strikerate;
module.exports.superovereco=superovereco;
module.exports.highestDismissal=highestDismissal;



