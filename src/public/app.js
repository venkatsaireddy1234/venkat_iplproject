console.log("Hi");
fetch('http://127.0.0.1:8080/src/public/output/matchesperyear.json')
    .then(response => response.json())
    .then(data => console.log(matchesperyeargraph(data)));

function matchesperyeargraph(matchesperyear){
const chart = Highcharts.chart('container1', {
    title: {
        text: 'Matches per year'
    },

    xAxis: {
        categories: Object.keys(matchesperyear)
    },
    series: [{
        type: 'column',
        colorByPoint: true,
        data: [58,57,60,73,74,76,60,59,60,59],
        showInLegend: false
    }]
    });
    
    document.getElementById('plain').addEventListener('click', () => {
    chart.update({
        chart: {
        inverted: false,
        polar: false
        },
        subtitle: {
        text: 'Plain'
        }
    });
    });
    
    document.getElementById('inverted').addEventListener('click', () => {
    chart.update({
        chart: {
        inverted: true,
        polar: false
        },
        subtitle: {
        text: 'Inverted'
        }
    });
    });
    
    document.getElementById('polar').addEventListener('click', () => {
    chart.update({
        chart: {
        inverted: false,
        polar: true
        },
        subtitle: {
        text: 'Polar'
        }
    });
    })
};

    

console.log("Hi");
fetch('http://127.0.0.1:8080/src/public/output/extraruns2016.json')
  .then(response => response.json())
  .then(data => console.log(extrarunsgraph(data)));
  

function extrarunsgraph(extraruns2016){
    Highcharts.chart('container2', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'ExtraRuns in 2016'
        },
        xAxis: {
          categories: Object.keys(extraruns2016),
          crosshair: true
        },
        yAxis: {
          title: {
            text: 'Extraruns in conceded in 2016  '
          }
        },
        
        series: [{
          name: "2016",
          data: Object.values(extraruns2016)
      
        },]
      });
};




console.log("Hi");
fetch('http://127.0.0.1:8080/src/public/output/topTenEconomy.json')
    .then(response => response.json())
    .then(data => console.log(toptenecograpgh(data)));

function toptenecograpgh(topTenEconomy){
    const chart = Highcharts.chart('container3', {
        title: {
          text: 'Topten economy bowlers in 2015'
        },
    
        xAxis: {
          categories: Object.keys(topTenEconomy)
        },
        series: [{
          type: 'column',
          colorByPoint: true,
          data: [3.01,3.75,4.14,5.61,5.72,5.96,6.20,6.23,6.42,6.70],
          showInLegend: false
        }]
      });
      
      document.getElementById('plain').addEventListener('click', () => {
        chart.update({
          chart: {
            inverted: false,
            polar: false
          },
          subtitle: {
            text: 'Plain'
          }
        });
      });
      
      document.getElementById('inverted').addEventListener('click', () => {
        chart.update({
          chart: {
            inverted: true,
            polar: false
          },
          subtitle: {
            text: 'Inverted'
          }
        });
      });
      
      document.getElementById('polar').addEventListener('click', () => {
        chart.update({
          chart: {
            inverted: false,
            polar: true
          },
          subtitle: {
            text: 'Polar'
          }
        });
      })
    };


console.log("Hi");
fetch('http://127.0.0.1:8080/src/public/output/matchesownperyear.json')
    .then(response => response.json())
    .then(data => console.log(matchesownperyeargraph(data)));
    

function matchesownperyeargraph(matchesownperyear){
    Highcharts.chart('container4', {

        chart: {
            type: 'column'
        },
        
        title: {
            text: 'Matches won per year'
        },
        
        xAxis: {
            categories: ['2008','2009','2010','2011','2012','2013','2014','2015','2016']
        },
        
        yAxis: {
            allowDecimals: false,
            title: {
            text: 'Number of wins'
            }
        },
        
        tooltip: {
            formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
            }
        },
        
        plotOptions: {
            column: {
            stacking: 'normal'
            }
        },
        
        series: [{
            name : "MI",
            data:  [6,5,7,4,13,10,2,7],
        
        }, {
            name : "RR",
            data: [9,7,10,9,8,3,6,7],
            
        }, {
            name : "DC",
            data: [7,11,7,8,8,9,6,4],
            
        }, {
            name : "SRH",
            data: [4,6,10,4,8,7,6,6,4,1],
        }, {
            name : "KKR",
            data: [13,11,4,7,8,10,12,8,4],
            }, {
            name : "KXIP",
            data: [10,9,10,11,,13,8,12,4,3],
            }, {
            name : "CSK",
            data: [2,5,12,7,2,10,6,7],
            }, {
            name : "KTK",
            data: [7,10,78,3,5,7,5,10,2]
            }, {
            name : "RCB",
            data: [5,8,9,9,7,7,4,11],
            
        }]
})
}
